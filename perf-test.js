const Benchmark = require('benchmark');
const Match = require('./dist/src/Matcher').default;
const isequalwith = require('lodash.isequalwith');

const sleepFor = (sleepDuration) => {
  const now = new Date().getTime();
  while (new Date().getTime() < now + sleepDuration) { }
};

const inputForDataSimulation = new Array(100).fill(0).map((_, i) => ({id: i, a: 1, b: 2}));
const simulateCreationDataForTableComponent = () => {
  return inputForDataSimulation.map(({id, a, b}) => ({id, A: a.toString(), B: `a + b = ${a + b}`}));
};

const runTest = (name, case1, case1f, case2, case2f) => {
  console.log('⌚ ' + name);
  new Benchmark.Suite()
    .add(case1, case1f)
    .add(case2, case2f)
    .on('cycle', event => console.log(String(event.target)))
    .run();
  console.log('');
};

runTest(
  'basic usage as switch',
  'native switch',
  () => {
    let res;
    switch (1) {
      case 0:
        res = 0;
        break;
      case 1:
        res = 1;
        break;
      default:
        res = -1;
    }
  },
  'matcher',
  () => {
    const res = Match(1)
      .case(0, () => 0)
      .case(1, () => 1)
      .default(() => -1)
      .exec();
  }
);

runTest(
  'multi',
  'native switch',
  () => {
    let res;
    switch (1) {
      case 0:
      case 1:
        res = 1;
        break;
    }
  },
  'matcher',
  () => {
    const res = Match(1)
      .caseMulti([0, 1], () => 1)
      .exec();
  }
);

runTest(
  'obj equality',
  'if + isequalwith',
  () => {
    const input = {a: 1, b: {c: 2}};
    let res;
    if (isequalwith(input, {a: 1, b: 2})) {
      res = 1;
    } else if (isequalwith(input, {a: 1, b: {c: 2}})) {
      res = 2;
    }
  },
  'matcher',
  () => {
    const res = Match({a: 1, b: {c: 2}})
      .case({a: 1, b: 2}, () => 1)
      .case({a: 1, b: {c: 2}}, () => 2)
      .exec();
  }
);

runTest(
  'multi-way if',
  'native if',
  () => {
    const a = {b: 1, c: {d: 2}};
    let res;
    if (a === 1) {
      res = 1;
    } else if (a.b === 2) {
      res = 2;
    } else if (a === 2 || a === 3 || a === 4) {
      res = 3;
    } else if (isequalwith(a, {}) || isequalwith(a, {b: 1}) || isequalwith(a, {c: 2})) {
      res = 4;
    } else if (isequalwith(a, {b: 1, c: {d: 2}})) {
      res = 5;
    }
  },
  'matcher',
  () => {
    const a = {b: 1, c: {d: 2}};
    const res = Match(a)
      .case(1, () => 1)
      .caseGuarded(x => x.b === 2, () => 2)
      .caseMulti([2, 3, 4], () => 3)
      .caseMulti([{}, {b: 1}, {c: 2}], () => 4)
      .case({b: 1, c: {d: 2}}, () => 5)
      .exec();
  }
);

runTest(
  'switch with data processing simulation',
  'native switch',
  () => {
    let res;
    switch (1) {
      case 0:
        res = 0;
        break;
      case 1:
        res = 1;
        simulateCreationDataForTableComponent();
        break;
      default:
        res = -1;
    }
  },
  'matcher',
  () => {
    const res = Match(1)
      .case(0, () => 0)
      .case(1, () => {
        simulateCreationDataForTableComponent();
        return 1;
      })
      .default(() => -1)
      .exec();
  }
);

runTest(
  'switch with work 1ms',
  'native switch',
  () => {
    let res;
    switch (1) {
      case 0:
        res = 0;
        break;
      case 1:
        res = 1;
        sleepFor(1);
        break;
      default:
        res = -1;
    }
  },
  'matcher',
  () => {
    const res = Match(1)
      .case(0, () => 0)
      .case(1, () => {
        sleepFor(1);
        return 1;
      })
      .default(() => -1)
      .exec();
  }
);
